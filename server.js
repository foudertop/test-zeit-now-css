'use strict';
var http = require('http');
var express = require("express");
var path = require("path");
var app = express();

app.use(express.static(path.join(__dirname, "/")));


app.get("/", (req, res) => {
    res.sendFile(__dirname + "/public/index.html");
});

app.listen(8080, () => {
    console.log("listening");
});